# Description : This code purpose is to distinguish blur face photo with clear face photo
# Contributor : Thariq Ramadhan


from argparse import ArgumentParser
from imutils import paths
import os
import numpy as np
import argparse
import cv2
import shutil
import tensorflow as tf
keras = tf.keras

# TO USE :
# python face_clustering_recursive.py \
# --input_dir <your-unprocessed-dataset-path> \
# --output_dir <your-processed-dataset-path> \
# --model <your-model>\```

parser = ArgumentParser()
parser.add_argument("--input_dir", type=str, default=None,
                    help="Path of raw dataset")
parser.add_argument("--output_dir", type=str, default=None,
                    help="Path of output directory")
parser.add_argument("--model", type=str, default=None,
                    help="The model of blur filter")
args = parser.parse_args()

def main():
    # List image path
    imagePaths = list(paths.list_images(args.input_dir))
    # Load model
    loadModel = keras.models.load_model(args.model)

    # Make output path
    blur_path = args.output_dir + "/blur"
    clear_path = args.output_dir + "/clear"
    try:
        os.mkdir(args.output_dir )
        print("Directory " , args.output_dir  ,  " Created ") 
    except FileExistsError:
        print("Directory " , args.output_dir  ,  " already exists")
    try:
        os.mkdir(blur_path)
        print("Directory " , blur_path ,  " Created ") 
    except FileExistsError:
        print("Directory " , blur_path ,  " already exists")
    try:
        os.mkdir(clear_path)
        print("Directory " , clear_path ,  " Created ") 
    except FileExistsError:
        print("Directory " , clear_path ,  " already exists")
    
    # Classify image
    i = 0
    for imagePath in imagePaths:
        isBlur =  predictBlur(imagePath, loadModel)
        if isBlur == 'blur':
            shutil.copyfile(imagePath, blur_path + '/' + str(i) +'.png')
        if isBlur == 'clear':
            shutil.copyfile(imagePath, clear_path + '/' + str(i) +'.png')
        i=i+1

def predictBlur(img_path, model):
  IMG_PATH = img_path
  print("Process image : " + IMG_PATH)
  data = []
  image = cv2.imread(IMG_PATH)
  image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
  image = cv2.resize(image, (224, 224))
  data.append(image)
  data = np.array(data) / 255.0
  preds = model.predict(data)
  i = np.argmax(preds[0])
  label = "blur" if i == 0 else "clear"
  return label

if __name__ == "__main__":
    main()