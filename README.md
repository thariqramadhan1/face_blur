# Face_Blur

This project purpose is distinguish blur face photo with clear face photo with transfer learning VGG16 model.
To use this code :

````python face_clustering_recursive.py \
--input_dir <your-unprocessed-dataset-path> \
--output_dir <your-processed-dataset-path> \
--model <your-model>\```
````
